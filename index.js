let express = require('express');
let levenshtein = require('js-levenshtein');
let app = express();
app.set('view engine', 'pug');

let users = ["gibbs4567", "abby1821", "dinozzo7392", "ziva8392", "mcgee7438", "vance7632", "ducky9823", "caitlin0239", "jimmy8302", "jenny4265"];
let interrogations = new Array(users.length).fill(0);
let nbRequests = 0

app.get("/", (req, res) => {
    res.render("homepage",{ title: 'TP9 - NCIS',
                            nbRequests: 'Nombre de requêtes : ' + nbRequests,
                            backgroundSrc: "https://pre00.deviantart.net/31b0/th/pre/f/2010/258/b/2/wallpaper___abby_by_nikky81-d2yrpdo.jpg",
                            formClient: "/ClientIA"});
});

app.get("/ClientIA", (req, res) => {
    res.render("formClient",{ title: 'TP9 - NCIS',
        nbRequests: 'Nombre de requêtes : ' + nbRequests,
        backgroundSrc: "https://pre00.deviantart.net/31b0/th/pre/f/2010/258/b/2/wallpaper___abby_by_nikky81-d2yrpdo.jpg",
        formClient: "/ClientIA"});
});

app.get("/nbRequests", (req, res) => {
    res.send(nbRequests.toString())
});

app.get("/:user/distance/:A/:B", (req, res) => {
    let user = req.params.user;
    let a = req.params.A;
    let b = req.params.B;

    if(users.includes(user)){
        if(interrogations[users.indexOf(user)] >= 5){
            res.json({"utilisateur":user,"erreur":"nombre de requêtes dépassé, attendez une minute"});
        }else{
            if(a.length > 50 || b.length > 50){
                res.json({"erreur":"une des deux chaînes est trop longue (gardez des chaînes inférieures à 50)"});
            }else{
                if((a+b).split(/[ACGT]+/).join('').length > 0){
                    res.json({"erreur":"une des chaînes ne code pas de l’ADN"});
                }else{
                    // Current date
                    let date = new Date();
                    let months = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
                    let currentDate = date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear() + " " + date.getHours() + "h" + ((date.getMinutes() > 10)? date.getMinutes() : "0"+date.getMinutes());

                    // Distance
                    let start = new Date().getTime();
                    let dist = levenshtein(a,b);
                    let end = new Date().getTime();

                    // Number of requests by minute
                    interrogations[users.indexOf(user)]++
                    nbRequests++

                    res.json({"utilisateur": user,
                        "date": currentDate,
                        "A": a,
                        "B": b,
                        "distance": dist,
                        "temps de calcul (ms)": (end - start),
                        "interrogations minute": interrogations[users.indexOf(user)]});
                }
            }
        }
    }else{
        res.json({"erreur":"vous n’avez pas les autorisations pour utiliser ce service"});
    }
});

setInterval(() => {
    interrogations = new Array(users.length).fill(0);
}, 60000);

app.get("*", (req, res) => {
    res.json({"erreur":"la requête est mal formée"})
});

const port = process.env.PORT || 5000;
app.listen(port, function () {
    console.log('app listening on port ' + port + '!');
});